<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Home_model
 *
 * @author k
 */
class Home_model extends CI_Model {

    public function get_facility_count($filters) {
        $this->db->select("County name,COUNT(facility)y, UPPER(County) drilldown", FALSE);
        if (!empty($filters)) {
            foreach ($filters as $category => $filter) {
                $this->db->where_in($category, $filter);
            }
        }
        $this->db->group_by('name');
        $this->db->order_by('y', 'Desc');
        $this->db->limit(50);
        $query = $this->db->get('tbl_facility_details');
        return $this->get_facility_count_drilldown(array('main' => $query->result_array()), $filters);
    }

    public function get_facility_count_drilldown($main_data, $filters) {
        $drilldown_data = array();
        $this->db->select("UPPER(County) category, Sub_County name,COUNT(facility)y", FALSE);
        if (!empty($filters)) {
            foreach ($filters as $category => $filter) {
                $this->db->where_in($category, $filter);
            }
        }
        $this->db->group_by('name');
        $this->db->order_by('y', 'Desc');
        $query = $this->db->get('tbl_facility_details');
        $sub_data = $query->result_array();

        if ($main_data) {
            foreach ($main_data['main'] as $counter => $main) {
                $category = $main['drilldown'];

                $drilldown_data['drilldown'][$counter]['id'] = $category;
                $drilldown_data['drilldown'][$counter]['name'] = ucwords($category);
                $drilldown_data['drilldown'][$counter]['colorByPoint'] = true;

                foreach ($sub_data as $sub) {
                    if ($category == $sub['category']) {
                        unset($sub['category']);
                        $drilldown_data['drilldown'][$counter]['data'][] = $sub;
                    }
                }
            }
        }
        return array_merge($main_data, $drilldown_data);
    }

    public function get_facility_names_drilldown($sub_county_name){
      $this->db->select("UPPER(facility)");
      $this->db->where("Sub_County", $sub_county_name);
      $this->db->group_by("facility");
      $query = $this->db->get('tbl_facility_details');
      $facility_data = $query->result_array();
      return $facility_data;
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['helper'] = array('url');
$autoload['config'] = array('charts');
$autoload['model'] = array('home_model');

<!--banner-->
<section id="banner" class="banner">
    <div class="bg-color">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="col-md-12">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"></a>
                    </div>
                    <div class="collapse navbar-collapse navbar-right" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#banner">Home</a></li>
                            <li class=""><a href="#prep_journey">PrEP Journey</a></li>
                            <li class=""><a href="#status_in_kenya">Status in Kenya</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    Resources
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="#guidelines_policies_tools">National Policies and Guidelines</a></li>
                                    <li><a href="#gallery_section">Gallery</a></li>
                                    <li><a href="#publications">Publications</a></li>
                                </ul>
                            </li>
                            <li class=""><a href="#faq">FAQ</a></li>
                            <li class=""><a href="#contact">Contact</a></li>
                            <li><a href="<?php echo base_url('manager'); ?>" onclick="window.location.reload(true);"><span class="glyphicon glyphicon-log-in"></span> login</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="row">
                <div class="banner-info">
                    <div class="text-left banner-text margin-top-md">
                        <p class="white">Vision</p>
                        <span class="pull-left"><hr class="botm-line-auto"></span>
                        <br/>
                        <h4 class="white"><i>Zero New Infections in Kenya</i></h4>
                        &nbsp;
                        <p class="white">Our Goal</p>
                        <span class="pull-left"><hr class="botm-line-auto"></span>
                        <br/>
                        <h4 class="white"><i>Contribute to ending the AIDS epidemic by 2030 and achieving <br/> universal access to comprehensive and highly effective HIV prevention</i></h4>
                        &nbsp;
                        &nbsp;
                        <p class="white">Mission</p>
                        <span class="pull-left"><hr class="botm-line-auto"></span>
                        <br/>
                        <h4 class="white"><i>To provide sustainable access to safe, affordable and quality PrEP services <br/> in combination with other HIV prevention interventions</i></h4>
                        <a href="#faq" class="btn btn-appoint">What is PrEP?</a>
                    </div>
                    <div class="overlay-detail text-left">
                        <a href="#service"><i class="fa fa-angle-down"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="prep_journey" class="banner">
    <div class="container-fluid">
        <div class="row">
        </div>
    </div>
</section>

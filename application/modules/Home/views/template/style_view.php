<!--favicon-->
<link rel="shortcut icon" type="text/css" href="<?php echo base_url() . 'public/home/img/favicon.ico'; ?>">
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans|Raleway|Candal">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'public/home/css/font-awesome.min.css'; ?>">
<!--bootstrap-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'public/home/lib/bootstrap/dist/css/bootstrap.min.css'; ?>">
<!--bootstrap-toggle-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'public/home/lib/bootstrap-toggle/css/bootstrap-toggle.min.css'; ?>">
<!--bootstrap-multiselect-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'public/dashboard/lib/bootstrap-multiselect/css/bootstrap-multiselect.css'; ?>">
<!--dataTable-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'public/dashboard/lib/datatables/css/datatables.min.css'; ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'public/dashboard/lib/datatables/css/jquery.dataTables.min.css'; ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'public/dashboard/lib/datatables/css/buttons.dataTables.min.css'; ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'public/dashboard/lib/datatables/css/dataTables.bootstrap.min.css'; ?>">
<!--daterangepicker-->
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url() . 'public/dashboard/lib/daterangepicker/css/daterangepicker.css'; ?>">
<!-- external libs from cdnjs -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.11/c3.min.css">
<!-- pivotTable-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'public/home/lib/pivotTable/css/pivot.css'; ?>">
<!--jquery-->
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/jquery/dist/jquery.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'public/home/lib/bootstrap/js/carousel.js'; ?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'public/home/css/style.css'; ?>">

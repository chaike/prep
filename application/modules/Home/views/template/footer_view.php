<footer id="footer">
    <div class="footer-line">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    &copy; <?php echo date('Y');?> www.nascop.or.ke . All Rights Reserved
                </div>
            </div>
        </div>
    </div>
</footer>
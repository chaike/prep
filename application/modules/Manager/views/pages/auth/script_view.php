<!-- jQuery -->
<script src="<?php echo base_url() . 'public/manager/lib/sbadmin2/vendor/jquery/jquery.min.js'; ?>"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url() . 'public/manager/lib/sbadmin2/vendor/bootstrap/js/bootstrap.min.js'; ?>"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url() . 'public/manager/lib/sbadmin2/vendor/metisMenu/metisMenu.min.js'; ?>"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url() . 'public/manager/lib/sbadmin2/dist/js/sb-admin-2.js'; ?>"></script>
<!--Axios Source-->
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<!--VueJS Source-->
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<!--Vue Code-->
<script src="<?php echo base_url(). 'public/manager/js/app.js'; ?>"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url() . 'public/manager/lib/sbadmin2/vendor/bootstrap/js/bootstrap.min.js'; ?>"></script>
<!--data tables-->
<script src="<?php echo base_url('public/user/datatables/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('public/user/datatables/js/dataTables.bootstrap.js') ?>"></script>
<script src="<?php echo base_url('public/user/bootstrap-datepicker/js/bootstrap-datepicker.min.js') ?>"></script>
<!--nav-bar drop-down js-->
<script type="text/javascript" src="<?php echo base_url() . 'public/manager/js/nav_dropdown.js'; ?>"></script>
<!--bootbox-->
<script type="text/javascript" src="<?php echo base_url() . 'public/manager/lib/bootbox/js/bootbox.min.js'; ?>"></script>
<!--data tables-->
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="https://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(function () {
        $('.<?= $page_name; ?>').addClass("active-page");
    });
</script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.17/vue.js"></script>


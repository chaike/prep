<link rel="shortcut icon" type="text/css" href="<?php echo base_url() . 'public/home/img/favicon.ico'; ?>">
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url() . 'public/manager/lib/sbadmin2/vendor/bootstrap/css/bootstrap.min.css'; ?>" rel="stylesheet">
<!-- MetisMenu CSS -->
<link href="<?php echo base_url() . 'public/manager/lib/sbadmin2/vendor/metisMenu/metisMenu.min.css'; ?>" rel="stylesheet">
<!-- Morris Charts CSS -->
<link href="<?php echo base_url() . 'public/manager/lib/sbadmin2/vendor/morrisjs/morris.css'; ?>" rel="stylesheet">
<!-- Custom Fonts -->
<link href="<?php echo base_url() . 'public/manager/lib/sbadmin2/vendor/font-awesome/css/font-awesome.min.css'; ?>" rel="stylesheet" type="text/css">
<!-- Sb-Manager CSS -->
<link href="<?php echo base_url() . 'public/manager/lib/sbadmin2/dist/css/sb-admin-2.css'; ?>" rel="stylesheet">
<!-- blueimp Gallery styles -->
<link rel="stylesheet" href="https://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<noscript><link rel="stylesheet" href="<?php echo base_url() . 'public/jQueryupload/css/jquery.fileupload-noscript.css'; ?>"></noscript>
<noscript><link rel="stylesheet" href="<?php echo base_url() . 'public/jQueryupload/css/jquery.fileupload-ui-noscript.css'; ?>"></noscript>
<style>
    /* Hide Angular JS elements before initializing */
    .ng-cloak {
        display: none;
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
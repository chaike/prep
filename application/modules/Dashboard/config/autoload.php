<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['helper'] = array('url');
$autoload['config'] = array('charts');
$autoload['model'] = array('Service_delivery_model','Partner_model','Laboratory_service_model','Human_resource_model','Commodity_management_model','Monitoring_evaluation_model','Communication_advocacy_model');
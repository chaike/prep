<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of Human_resource_model
 *
 * @author Marete
 */
class Human_resource_model extends CI_Model {

    public function get_distibution_of_facilities_trained_personnel($filters) {
        $this->db->select("trained_on_prep name,Count(*)y, UPPER(REPLACE(trained_on_prep, ' ', '_')) drilldown", FALSE);
        if (!empty($filters)) {
            foreach ($filters as $category => $filter) {
                $this->db->where_in($category, $filter);
            }
        }
        $this->db->group_by('name');
        $this->db->order_by('y', 'Desc');
        $query = $this->db->get('tbl_trained_personnel');
        return $this->get_distibution_of_facilities_trained_personnel_drilldown(array('main' => $query->result_array()), $filters);
    }

    public function get_distibution_of_facilities_trained_personnel_drilldown($main_data, $filters) {
        $drilldown_data = array();
        $this->db->select("UPPER(REPLACE(trained_on_prep, ' ', '_')) category, County name,COUNT(*)y, UPPER(CONCAT_WS('_', REPLACE(trained_on_prep, ' ', '_'), REPLACE(County, ' ', '_'))) drilldown, '#7798BF' color", FALSE);
        if (!empty($filters)) {
            foreach ($filters as $category => $filter) {
                $this->db->where_in($category, $filter);
            }
        }
        $this->db->group_by('drilldown');
        $this->db->order_by('y', 'Desc');
        $query = $this->db->get('tbl_trained_personnel');
        $sub_data = $query->result_array();

        if ($main_data) {
            foreach ($main_data['main'] as $counter => $main) {
                $category = $main['drilldown'];

                $drilldown_data['drilldown'][$counter]['id'] = $category;
                $drilldown_data['drilldown'][$counter]['name'] = ucwords($category);
                $drilldown_data['drilldown'][$counter]['colorByPoint'] = true;

                foreach ($sub_data as $sub) {
                    if ($category == $sub['category']) {
                        unset($sub['category']);
                        $drilldown_data['drilldown'][$counter]['data'][] = $sub;
                    }
                }
            }
        }
        $drilldown_data = $this->get_distibution_of_facilities_trained_personnel_drilldown_level2($drilldown_data, $filters);
        return array_merge($main_data, $drilldown_data);
    }

    public function get_distibution_of_facilities_trained_personnel_drilldown_level2($drilldown_data, $filters) {
        $this->db->select("UPPER(CONCAT_WS('_', REPLACE(trained_on_prep, ' ', '_'), REPLACE(County, ' ', '_'))) category, Sub_County name, COUNT(*)y, UPPER(CONCAT_WS('_', CONCAT_WS('_', REPLACE(trained_on_prep, ' ', '_'), REPLACE(County, ' ', '_')), REPLACE(Sub_County, ' ', '_'))) drilldown, '#90ee7e' color", FALSE);
        if (!empty($filters)) {
            foreach ($filters as $category => $filter) {
                $this->db->where_in($category, $filter);
            }
        }
        $this->db->group_by('name');
        $this->db->order_by('y', 'DESC');
        $query = $this->db->get('tbl_trained_personnel');
        $population_data = $query->result_array();

        if ($drilldown_data) {
            $counter = sizeof($drilldown_data['drilldown']);
            foreach ($drilldown_data['drilldown'] as $main_data) {
                foreach ($main_data['data'] as $item) {
                    $filter_value = $item['name'];
                    $filter_name = $item['drilldown'];

                    $drilldown_data['drilldown'][$counter]['id'] = $filter_name;
                    $drilldown_data['drilldown'][$counter]['name'] = ucwords($filter_name);
                    $drilldown_data['drilldown'][$counter]['colorByPoint'] = true;

                    foreach ($population_data as $population) {
                        if ($filter_name == $population['category']) {
                            unset($population['category']);
                            $drilldown_data['drilldown'][$counter]['data'][] = $population;
                        }
                    }
                    $counter += 1;
                }
            }
        }
        return $this->get_distibution_of_facilities_trained_personnel_drilldown_level3($drilldown_data, $filters);
    }

    public function get_distibution_of_facilities_trained_personnel_drilldown_level3($drilldown_data, $filters){
        $this->db->select("UPPER(CONCAT_WS('_', CONCAT_WS('_', REPLACE(trained_on_prep, ' ', '_'), REPLACE(County, ' ', '_')), REPLACE(Sub_County, ' ', '_'))) category, facility name, COUNT(*)y, '#dabdab' color", FALSE);
        if (!empty($filters)) {
            foreach ($filters as $category => $filter) {
                $this->db->where_in($category, $filter);
            }
        }
        $this->db->group_by('category, name');
        $this->db->order_by('y', 'DESC');
        $query = $this->db->get('tbl_trained_personnel');
        $facility_data = $query->result_array();

        if ($drilldown_data) {
            $counter = sizeof($drilldown_data['drilldown']);
            foreach ($drilldown_data['drilldown'] as $main_data) {
                if(!empty($main_data['data'])){
                    foreach ($main_data['data'] as $item) {
                        $filter_name = $item['drilldown'];
                        foreach ($facility_data as $facility) {
                            if ($filter_name == $facility['category']) {
                                unset($facility['category']);
                                $drilldown_data['drilldown'][$counter]['id'] = $filter_name;
                                $drilldown_data['drilldown'][$counter]['name'] = ucwords($filter_name);
                                $drilldown_data['drilldown'][$counter]['colorByPoint'] = true;
                                $drilldown_data['drilldown'][$counter]['data'][] = $facility;
                            }
                        }
                        $counter += 1;
                    }
                }
            }
        }
        return $drilldown_data;
    }

    public function get_cadre_trained($filters) {
        $columns = array();
        $this->db->select("Cadre name,COUNT(*)y", FALSE);
        if (!empty($filters)) {
            foreach ($filters as $category => $filter) {
                $this->db->where_in($category, $filter);
            }
        }
        $this->db->group_by('name');
        $this->db->order_by('y', 'DESC');
        $query = $this->db->get('tbl_cadre');
        $results = $query->result_array();

        foreach ($results as $result) {
            array_push($columns, $result['name']);
        }

        return array('main' => $results, 'columns' => $columns);
    }

    public function get_health_care_workers_trained_on_prep_numbers($filters) {
        $columns = array();
        $this->db->select("CASE WHEN (hcw_trained_on_prep > 0 AND hcw_trained_on_prep <= 3) THEN '1-3' WHEN (hcw_trained_on_prep > 3 AND hcw_trained_on_prep <= 6) THEN '4-6' WHEN (hcw_trained_on_prep > 7) THEN '>7' ELSE NULL END AS name, CASE WHEN (hcw_trained_on_prep > 0 AND hcw_trained_on_prep <= 3) THEN COUNT(1) WHEN (hcw_trained_on_prep > 3 AND hcw_trained_on_prep <= 6) THEN COUNT(1) WHEN (hcw_trained_on_prep > 7) THEN COUNT(1) ELSE NULL END AS y", FALSE);
        if (!empty($filters)) {
            foreach ($filters as $category => $filter) {
                $this->db->where_in($category, $filter);
            }
        }
        $this->db->group_by('name');
        $this->db->order_by('y', 'DESC');
        $query = $this->db->get('tbl_trained_personnel');
        $results = $query->result_array();

        foreach ($results as $result) {
            array_push($columns, $result['name']);
        }

        return array('main' => $results, 'columns' => $columns); 
    }

    public function get_health_care_workers_trained_on_prep($filters) {
        $columns = array();
        $hcw_trained_data = array(
            array('type' => 'column', 'name' => '1-3', 'data' => array()),
            array('type' => 'column', 'name' => '4-6', 'data' => array()),
            array('type' => 'column', 'name' => '>7', 'data' => array()),
        );

        $this->db->select("UPPER(County) county, COUNT(IF(hcw_trained_on_prep > 0, 1, NULL) AND IF(hcw_trained_on_prep <= 3, 1, NULL)) '1-3', COUNT(IF(hcw_trained_on_prep > 3, 1, NULL) AND IF(hcw_trained_on_prep <= 6, 1, NULL)) '4-6', COUNT(IF(hcw_trained_on_prep > 6, 1, NULL)) '>7'", FALSE);
        if (!empty($filters)) {
            foreach ($filters as $category => $filter) {
                $this->db->where_in($category, $filter);
            }
        }
        $this->db->group_by('county');
        $query = $this->db->get('tbl_trained_personnel');
        $results = $query->result_array();

        if ($results) {
            foreach ($results as $result) {
                $columns[] = $result['county'];
                foreach ($hcw_trained_data as $index => $hcw_trained) {
                    if ($hcw_trained['name'] == '>7') {
                        array_push($hcw_trained_data[$index]['data'], $result['>7']);
                    } else if ($hcw_trained['name'] == '4-6') {
                        array_push($hcw_trained_data[$index]['data'], $result['4-6']);
                    } else if ($hcw_trained['name'] == '1-3') {
                        array_push($hcw_trained_data[$index]['data'], $result['1-3']);
                    }
                }
            }
        }
        return array('main' => $hcw_trained_data, 'columns' => $columns);
    }

}

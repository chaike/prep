<!--highcharts-->
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/highmaps/js/highcharts.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/highmaps/js/map.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/highmaps/js/data.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/highmaps/js/cn-all.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/highmaps/js/exporting.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/highmaps/js/offline-exporting.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/highmaps/js/drilldown.js';?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/highmaps/js/us-all.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/highmaps/js/export-data.js'; ?>"></script>
<!--bootstrap-->
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/bootstrap/dist/js/bootstrap.min.js'; ?>"></script>
<!--bootstrap-toggle-->
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/bootstrap-toggle/js/bootstrap-toggle.min.js'; ?>"></script>
<!--spin-->
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/js/spin.min.js'; ?>"></script>
<!--bootstrap-multiselect-->
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/bootstrap-multiselect/js/bootstrap-multiselect.js'; ?>"></script>
<!--disable_back_button-->
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/js/disable_back_button.js'; ?>"></script>
<!--datatables-->
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/datatables/js/datatables.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/datatables/js/dataTables.buttons.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/datatables/js/buttons.flash.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/datatables/js/jszip.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/datatables/js/pdfmake.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/datatables/js/vfs_fonts.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/datatables/js/buttons.html5.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/datatables/js/buttons.print.min.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/datatables/js/dataTables.bootstrap.min.js'; ?>"></script>
<!--moment-->
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/moment/js/moment.min.js'; ?>"></script>
<!--daterangepicker-->
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/daterangepicker/js/daterangepicker.js' ?>"></script>
<!--bootbox-->
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/bootbox/js/bootbox.min.js'; ?>"></script>
<!-- external libs from cdnjs -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.11/c3.min.js"></script>
<!-- pivotTable-->
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/pivotTable/js/pivot.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/pivotTable/js/export_renderers.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/lib/pivotTable/js/d3_renderers.js'; ?>"></script>
<!--dashboard-->
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.17/vue.js"></script>
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/js/dashboard.js'; ?>"></script>
<script type="text/javascript" src="<?php echo base_url() . 'public/dashboard/js/chatkit.js'; ?>"></script>



<div role="tabpanel" class="tab-pane" id="partner">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>
                            Availability of implementing partner support <span class="label label-warning">Drilldown</span>
                        </strong>
                    </div>
                    <div class="chart-stage">
                        <div id="partner_support_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="partner_support_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div><!--end row-->
        <div class="row">
            <div class="col-md-12">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>
                            Distribution of Partners (By County/SubCounty) <span class="label label-warning">Drilldown</span>
                        </strong>
                    </div>
                    <div class="chart-stage">
                        <div id="partner_distribution_map"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="partner_distribution_map_heading heading"></span>
                    </div>
                </div>
            </div>
        </div><!--end row-->
        <div class="row">
            <div class="col-md-12">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>
                            Distribution of facilities supported by partners <span class="label label-warning">Drilldown</span>
                        </strong>
                    </div>
                    <div class="chart-stage">
                        <div id="partner_facility_table"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="partner_facility_table_heading heading"></span>
                    </div>
                </div>
            </div>
        </div><!--end row-->
        <div class="row">
            <div class="col-md-12">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>Service Delivery Points by Partners</strong>
                    </div>
                    <div class="chart-stage">
                        <div id="service_delivery_point_by_partner_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="service_delivery_point_by_partner_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div><!--end row-->
        <div class="row">
            <div class="col-md-12">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>
                            Distribution of facilities that have trained staff by partner  <span class="label label-warning">Drilldown</span>
                        </strong>
                    </div>
                    <div class="chart-stage">
                        <div id="hcw_trained_by_partner_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="hcw_trained_by_partner_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div><!--end row-->
    </div>
</div>
<div role="tabpanel" class="tab-pane" id="laboratory_hep_c">
    <div class="container-fluid">
        <div class="row">          
            <div class="col-md-6">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>Access to Hep-C Testing in Facilities <span class="label label-warning">Drilldown</span></strong>
                    </div>
                    <div class="chart-stage">
                        <div id="access_hep_c_testing_facilities_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="access_hep_c_testing_facilities_chart_heading heading"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>Availability of Offsite vs Onsite Hep-C Testing <span class="label label-warning">Drilldown</span></strong>
                    </div>
                    <div class="chart-stage">
                        <div id="offsite_onsite_hep_c_testing_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="offsite_onsite_hep_c_testing_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div><!--end row-->
        <div class="row">
            <div class="col-md-6">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>Availability of Hep-C Testing Equipment <span class="label label-warning">Drilldown</span></strong>
                    </div>
                    <div class="chart-stage">
                        <div id="hep_c_testing_equipment_availability_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="hep_c_testing_equipment_availability_chart_heading heading"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>Availability of Hep-C Reagents Facilities With Equipment <span class="label label-warning">Drilldown</span></strong>
                    </div>
                    <div class="chart-stage">
                        <div id="hep_c_reagents_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="hep_c_reagents_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div><!--end row-->
    </div>
</div>
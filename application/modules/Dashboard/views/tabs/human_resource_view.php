<div role="tabpanel" class="tab-pane" id="human_resource">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>Distribution of facilities by number of staff trained  <span class="label label-warning">Drilldown</span></strong>
                    </div>
                    <div class="chart-stage">
                        <div id="distibution_of_facilities_trained_personnel_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="distibution_of_facilities_trained_personnel_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div><!--end row-->
        <div class="row">
            <div class="col-md-12">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>Distribution of Staff Trained by Cadre</strong>
                    </div>
                    <div class="chart-stage">
                        <div id="cadre_trained_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="cadre_trained_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div><!--end row-->
        <div class="row">
            <div class="col-md-4">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>Distribution of facilities by number of HCW trained (National)</strong>
                    </div>
                    <div class="chart-stage">
                        <div id="health_care_workers_trained_on_prep_table"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="health_care_workers_trained_on_prep_table_heading heading"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>Distribution of facilities by number of HCW trained (By County)</strong>
                    </div>
                    <div class="chart-stage">
                        <div id="health_care_workers_trained_on_prep_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="health_care_workers_trained_on_prep_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div><!--end row-->
    </div>
</div>
<div role="tabpanel" class="tab-pane" id="monitoring_evaluation">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>
                            Availability of LMIS Tools <span class="label label-warning">Drilldown</span>
                        </strong>
                    </div>
                    <div class="chart-stage">
                        <div id="lmis_tools_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="lmis_tools_chart_heading heading"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>
                            Availability of Clinical Encounter Forms <span class="label label-warning">Drilldown</span>
                        </strong>
                    </div>
                    <div class="chart-stage">
                        <div id="clinical_encounter_forms_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="clinical_encounter_forms_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div><!--end row-->
        <div class="row">
            <div class="col-md-6">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>
                            Availability of Pharmacovigilance Tools <span class="label label-warning">Drilldown</span>
                        </strong>
                    </div>
                    <div class="chart-stage">
                        <div id="pharmacovigilance_tools_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="pharmacovigilance_tools_chart_heading heading"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>
                            Availability of PrEP Register <span class="label label-warning">Drilldown</span>
                        </strong>
                    </div>
                    <div class="chart-stage">
                        <div id="prep_register_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="prep_register_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div><!--end row-->
        <div class="row">
            <div class="col-md-6">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>
                            Availability of Rapid Assessment Screening Tools <span class="label label-warning">Drilldown</span>
                        </strong>
                    </div>
                    <div class="chart-stage">
                        <div id="rapid_assessment_screening_tools_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="rapid_assessment_screening_tools_chart_heading heading"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>
                            Availability of PrEP Summary Tools <span class="label label-warning">Drilldown</span>
                        </strong>
                    </div>
                    <div class="chart-stage">
                        <div id="prep_summary_tools_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="prep_summary_tools_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div><!--end row-->
        <div class="row">
            <div class="col-md-12">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>Clients ever on PrEP as at  February 2018</strong>
                    </div>
                    <div class="chart-stage">
                        <div id="clients_on_prep_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="clients_on_prep_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div><!--end row-->
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>Clients currently on PrEP as at <?php echo date('M Y', strtotime('-2 months')) ?></strong>
                    </div>
                    <div class="chart-stage">
                        <div id="dhis_clients_on_prep_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="dhis_clients_on_prep_chart_heading heading"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div><!--end row-->
    </div>
</div>
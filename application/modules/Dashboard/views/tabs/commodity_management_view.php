<div role="tabpanel" class="tab-pane" id="commodity_management">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>Facility Source of ARVs(National)</strong>
                    </div>
                    <div class="chart-stage">
                        <div id="facility_source_of_ARVs_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="facility_source_of_ARVs_chart_heading heading"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>Facility Source of ARVs(By County)</strong>
                    </div>
                    <div class="chart-stage">
                        <div id="facility_source_of_arvs_by_county_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="facility_source_of_arvs_by_county_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div><!--end row-->
        <div class="row">
            <div class="col-md-4">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>PrEP Dispensing Points in Facilities (National)</strong>
                    </div>
                    <div class="chart-stage">
                        <div id="prep_dispensing_points_table"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="prep_dispensing_points_table_heading heading"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>PrEP Dispensing Points in Facilities (By County)</strong>
                    </div>
                    <div class="chart-stage">
                        <div id="prep_dispensing_points_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="prep_dispensing_points_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div><!--end row-->
        <div class="row">
            <div class="col-md-4">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>PrEP Product Dispensed in Facilities (National)</strong>
                    </div>
                    <div class="chart-stage">
                        <div id="prep_product_dispensed_table"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="prep_product_dispensed_table_heading heading"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>PrEP Product Dispensed in Facilities (By County)</strong>
                    </div>
                    <div class="chart-stage">
                        <div id="prep_product_dispensed_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="prep_product_dispensed_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div><!--end row-->
        <div class="row">
            <div class="col-md-4">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>PrEP Dispensing Software in Facilities (National)</strong>
                    </div>
                    <div class="chart-stage">
                        <div id="prep_dispensing_software_table"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="prep_dispensing_software_table_heading heading"></span>
                    </div>
                </div>
            </div>            
            <div class="col-md-8">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>PrEP Dispensing Software in Facilities (By County)</strong>
                    </div>
                    <div class="chart-stage">
                        <div id="prep_dispensing_software_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="prep_dispensing_software_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div><!--end row-->
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>MoH 729A Central Site/Sub County- MAPS  Revision 2017 as at <?php echo date('M Y', strtotime('-2 months')) ?></strong>
                    </div>
                    <div class="chart-stage">
                        <div id="central_site_maps_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="central_site_maps_chart_heading heading"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div><!--end row-->
    </div>
</div>
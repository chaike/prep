<div role="tabpanel" class="tab-pane" id="laboratory_creatinine">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>Access to Creatinine Testing in Facilities <span class="label label-warning">Drilldown</span> </strong>
                    </div>
                    <div class="chart-stage">
                        <div id="access_creatinine_testing_facilities_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="access_creatinine_testing_facilities_chart_heading heading"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>Availability of Offsite vs Onsite Creatinine Testing <span class="label label-warning">Drilldown</span> </strong>
                    </div>
                    <div class="chart-stage">
                        <div id="offsite_onsite_creatinine_testing_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="offsite_onsite_creatinine_testing_chart_heading heading"></span>
                    </div>
                </div>
            </div>          
        </div><!--end row-->
        <div class="row">
            <div class="col-md-6">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>Availability of Creatinine Testing Equipment <span class="label label-warning">Drilldown</span> </strong>
                    </div>
                    <div class="chart-stage">
                        <div id="creatinine_testing_equipment_availability_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="creatinine_testing_equipment_availability_chart_heading heading"></span>
                    </div>
                </div>
            </div>  
            <div class="col-md-6">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>Availability of Creatinine Reagents Facilities With Equipment <span class="label label-warning">Drilldown</span> </strong>
                    </div>
                    <div class="chart-stage">
                        <div id="creatinine_reagents_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="creatinine_reagents_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div><!--end row-->
    </div>
</div>
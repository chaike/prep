<div role="tabpanel" class="tab-pane" id="laboratory_hep_b">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>Access to Hep-B Testing in Facilities <span class="label label-warning">Drilldown</span></strong>
                    </div>
                    <div class="chart-stage">
                        <div id="access_hep_b_testing_facilities_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="access_hep_b_testing_facilities_chart_heading heading"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>Availability of Offsite vs Onsite Hep-B Testing <span class="label label-warning">Drilldown</span></strong>
                    </div>
                    <div class="chart-stage">
                        <div id="offsite_onsite_hep_b_testing_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="offsite_onsite_hep_b_testing_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div><!--end row-->
        <div class="row">
            <div class="col-md-6">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>Availability of Hep-B Testing Equipment <span class="label label-warning">Drilldown</span></strong>
                    </div>
                    <div class="chart-stage">
                        <div id="hep_b_testing_equipment_availability_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="hep_b_testing_equipment_availability_chart_heading heading"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>Availability of Hep-B Reagents Facilities With Equipment <span class="label label-warning">Drilldown</span></strong>
                    </div>
                    <div class="chart-stage">
                        <div id="hep_b_reagents_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="hep_b_reagents_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div><!--end row-->
    </div>
</div>
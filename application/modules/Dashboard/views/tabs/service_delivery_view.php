<div role="tabpanel" class="tab-pane active" id="service_delivery">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>
                            Distribution of Facilities (By County/SubCounty) <span class="label label-warning">Drilldown</span>
                        </strong>
                    </div>
                    <div class="chart-stage">
                        <div id="facility_distribution_map"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="facility_distribution_map_heading heading"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>
                            Distribution of Facilities (By Level) <span class="label label-warning">Drilldown</span>
                        </strong>
                    </div>
                    <div class="chart-stage">
                        <div id="facilities_level_distribution_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="facilities_level_distribution_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>
                            PrEP Focal Person in Facilities <span class="label label-warning">Drilldown</span>
                        </strong>
                    </div>
                    <div class="chart-stage">
                        <div id="prep_focal_person_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="prep_focal_person_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div><!--end row-->
        <div class="row">
            <div class="col-md-12">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>
                            Distribution of HIV Services in Facilities <span class="label label-warning">Drilldown</span>
                        </strong>
                    </div>
                    <div class="chart-stage">
                        <div id="hiv_services_offered_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="hiv_services_offered_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>
                            Distribution of Current Service Delivery Points <span class="label label-warning">Drilldown</span>
                        </strong>
                    </div>
                    <div class="chart-stage">
                        <div id="current_service_delivery_points_distribution_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="current_service_delivery_points_distribution_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>
                            Provision of PrEP to different population types by Facility <span class="label label-warning">Drilldown</span>
                        </strong>
                    </div>
                    <div class="chart-stage">
                        <div id="population_receiving_prep_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="population_receiving_prep_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div><!--end row-->
    </div>
</div>
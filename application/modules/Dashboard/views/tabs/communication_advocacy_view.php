<div role="tabpanel" class="tab-pane" id="communication_advocacy">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>Availability of Demand Creation Activities in Facilities</strong>
                    </div>
                    <div class="chart-stage">
                        <div id="demand_creation_activities_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="demand_creation_activities_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div><!--end row-->
        <div class="row">
            <div class="col-md-12">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>Availability of Demand Creation Activities in the Community</strong>
                    </div>
                    <div class="chart-stage">
                        <div id="prep_education_availability_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="prep_education_availability_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div><!--end row-->
        <div class="row">
            <div class="col-md-12">
                <div class="chart-wrapper">
                    <div class="chart-title">
                        <strong>Availability of IEC Materials</strong>
                    </div>
                    <div class="chart-stage">
                        <div id="iec_materials_chart"></div>
                    </div>
                    <div class="chart-notes">
                        <span class="iec_materials_chart_heading heading"></span>
                    </div>
                </div>
            </div>
        </div><!--end row-->
    </div>
</div>
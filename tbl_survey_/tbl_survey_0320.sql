-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 20, 2019 at 06:32 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prep`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_surveys`
--

DROP TABLE IF EXISTS `tbl_surveys`;
CREATE TABLE `tbl_surveys` (
  `id` int(11) NOT NULL,
  `survey_title` varchar(50) NOT NULL,
  `survey_description` text NOT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `publish_status` int(11) NOT NULL DEFAULT '0',
  `publish_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_surveys`
--

INSERT INTO `tbl_surveys` (`id`, `survey_title`, `survey_description`, `start_date`, `publish_status`, `publish_date`, `end_date`) VALUES
(1, 'Survey Name 2', 'Description 4', '2019-02-12 11:08:15', 0, '2019-03-20 12:05:17', '2020-02-12 11:08:15'),
(2, 'Survey Name 2', 'Description 4 sfdfdfsd fsdfsdf', '2019-02-13 07:14:34', 0, '2019-03-20 12:05:17', '2020-02-13 07:14:34');

--
-- Triggers `tbl_surveys`
--
DROP TRIGGER IF EXISTS `endDate`;
DELIMITER $$
CREATE TRIGGER `endDate` BEFORE INSERT ON `tbl_surveys` FOR EACH ROW SET NEW.end_date = TIMESTAMPADD(YEAR, 1, NOW())
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_survey_answers`
--

DROP TABLE IF EXISTS `tbl_survey_answers`;
CREATE TABLE `tbl_survey_answers` (
  `id` int(11) NOT NULL,
  `survey_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `choice_id` int(11) NOT NULL,
  `answer_text` int(11) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_survey_choices`
--

DROP TABLE IF EXISTS `tbl_survey_choices`;
CREATE TABLE `tbl_survey_choices` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `choice_text` varchar(200) NOT NULL,
  `choice_weight` int(11) NOT NULL,
  `choice_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_survey_choices`
--

INSERT INTO `tbl_survey_choices` (`id`, `question_id`, `choice_text`, `choice_weight`, `choice_status`, `created_at`, `updated_at`) VALUES
(1, 1, '', 1, 1, '2019-03-19 16:01:06', '2019-03-19 16:01:06'),
(2, 2, '', 1, 1, '2019-03-19 16:02:29', '2019-03-19 16:02:29'),
(3, 3, '', 1, 1, '2019-03-19 16:22:45', '2019-03-19 16:22:45'),
(4, 4, '', 1, 1, '2019-03-19 16:26:19', '2019-03-19 16:26:19'),
(5, 5, '', 1, 1, '2019-03-19 16:26:29', '2019-03-19 16:26:29'),
(6, 6, 'tbl_partner', 1, 1, '2019-03-19 16:26:57', '2019-03-19 16:26:57'),
(7, 7, 'sfdf', 1, 1, '2019-03-19 16:27:22', '2019-03-19 16:27:22'),
(8, 7, 'sfsdf', 2, 1, '2019-03-19 16:27:22', '2019-03-19 16:27:22'),
(9, 8, 'tbl_prep_facilities', 1, 1, '2019-03-19 16:28:07', '2019-03-19 16:28:07'),
(10, 9, '', 1, 1, '2019-03-19 16:28:41', '2019-03-19 16:28:41'),
(11, 10, '', 1, 1, '2019-03-19 16:29:14', '2019-03-19 16:29:14'),
(12, 11, 'No it is ', 1, 1, '2019-03-20 11:46:26', '2019-03-20 11:46:26'),
(13, 11, 'Yes it is', 2, 1, '2019-03-20 11:46:26', '2019-03-20 11:46:26'),
(14, 11, 'Yes they are', 3, 1, '2019-03-20 11:46:26', '2019-03-20 11:46:26'),
(15, 12, 'sffsfs', 1, 1, '2019-03-20 11:54:36', '2019-03-20 11:54:36'),
(16, 12, 'sfsfsfdfdfd', 2, 1, '2019-03-20 11:54:36', '2019-03-20 11:54:36'),
(17, 13, 'tbl_subcounty', 1, 1, '2019-03-20 11:55:09', '2019-03-20 11:55:09');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_survey_lists`
--

DROP TABLE IF EXISTS `tbl_survey_lists`;
CREATE TABLE `tbl_survey_lists` (
  `id` int(11) NOT NULL,
  `list` varchar(50) NOT NULL,
  `tbl` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_survey_lists`
--

INSERT INTO `tbl_survey_lists` (`id`, `list`, `tbl`, `type`) VALUES
(1, 'Counties', 'tbl_county', 'location'),
(2, 'Sub Counties', 'tbl_subcounty', 'location'),
(3, 'Partners', 'tbl_partner', 'organizations');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_survey_questions`
--

DROP TABLE IF EXISTS `tbl_survey_questions`;
CREATE TABLE `tbl_survey_questions` (
  `id` int(11) NOT NULL,
  `survey_id` int(11) NOT NULL,
  `question_text` text NOT NULL,
  `choice_type` varchar(50) NOT NULL,
  `multichoice_type` varchar(11) DEFAULT NULL,
  `question_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_survey_questions`
--

INSERT INTO `tbl_survey_questions` (`id`, `survey_id`, `question_text`, `choice_type`, `multichoice_type`, `question_status`, `created_at`) VALUES
(1, 1, 'What is prose', 'Prose', NULL, 1, '2019-03-19 16:01:06'),
(2, 1, 'dgfgdgfdfg', 'Prose', NULL, 1, '2019-03-19 16:02:29'),
(3, 1, 'What is prose 456', 'Prose', NULL, 1, '2019-03-19 16:22:45'),
(4, 1, 'sfsdfsfsdf', 'Prose', NULL, 1, '2019-03-19 16:26:19'),
(5, 1, 'sfsfafsdfdf', 'Prose', NULL, 1, '2019-03-19 16:26:29'),
(6, 1, 'what is this list', 'List', NULL, 1, '2019-03-19 16:26:57'),
(7, 1, 'what is this list', 'Multichoice', 'Radio', 1, '2019-03-19 16:27:22'),
(8, 1, 'what is this facility', 'List', NULL, 1, '2019-03-19 16:28:07'),
(9, 1, 'what is this facility', 'Prose', NULL, 1, '2019-03-19 16:28:41'),
(10, 1, 'what is this facility 3', 'Prose', NULL, 1, '2019-03-19 16:29:13'),
(11, 1, 'Is this a multichoice question', 'Multichoice', 'Checkbox', 1, '2019-03-20 11:46:26'),
(12, 1, 'Testing whether multichoice on created works', 'Multichoice', 'Checkbox', 1, '2019-03-20 11:54:36'),
(13, 1, 'Which sub county is he from', 'List', NULL, 1, '2019-03-20 11:55:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_surveys`
--
ALTER TABLE `tbl_surveys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_survey_answers`
--
ALTER TABLE `tbl_survey_answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_survey_choices`
--
ALTER TABLE `tbl_survey_choices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_survey_lists`
--
ALTER TABLE `tbl_survey_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_survey_questions`
--
ALTER TABLE `tbl_survey_questions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_surveys`
--
ALTER TABLE `tbl_surveys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_survey_answers`
--
ALTER TABLE `tbl_survey_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_survey_choices`
--
ALTER TABLE `tbl_survey_choices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tbl_survey_lists`
--
ALTER TABLE `tbl_survey_lists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_survey_questions`
--
ALTER TABLE `tbl_survey_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

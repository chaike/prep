//Get URL
var url = new URL(window.location.href);



            Vue.component('list-component',{
                data:function(){
                    return{
                        listoptions:[]
                    }
                },
                mounted(){
                    this.getListsForQuestion();
                },
                methods:{
                    getListsForQuestion(){
                        var self = this;
                        axios.get(url.origin+'/prep/manager/survey/getListsForQuestion/'+this.question_id).
                        then(function(response){
                            self.listoptions = response.data;
                        }).catch(function(error){
                            return error
                        })
                    }   
                },
                props:['question_id'],
                template:'<div><select><option v-for="item in listoptions" value="item.id">{{item.name}}</option></select></div>'
            })


            Vue.component('choice-component',{
                data:function(){
                    return{
                        choices:[],
                        choice_type:''
                    }
                },
                mounted(){
                    this.getChoicesForQuestion();
                },
                methods:{
                    getChoicesForQuestion(){
                        var self=this;
                        axios.get(url.origin+'/prep/manager/survey/getChoicesForQuestion/'+this.question_id).then(function(response){
                            self.choices = response.data.choices;
                            self.choice_type = response.data.choice_type[0].multichoice_type;
                        }).catch(function(error){
                            return error;
                        })
                    }
                },
                props:['question_id'],
                template:`<div>
                            <div v-if="choice_type==='Radio'">
                                <div v-for="choice in choices">
                                    <label class="radio-inline"><input :type="choice_type" :name="choice_type+question_id" :value="choice.choice_text">&nbsp;{{choice.choice_text}}</label>
                                </div>
                            </div>
                            <div v-if="choice_type==='Checkbox'">
                                <div v-for="choice in choices">
                                    <label class="checkbox-inline"><input :type="choice_type" :name="choice_type+question_id+choice.choice_weight" :value="choice.choice_text">&nbsp;{{choice.choice_text}}</label>
                                </div>
                            </div>
                        </div>
                            `
            })

            //define component to hold answer type options
            Vue.component('answer-type-options', {
                data:function(){
                    return {
                        options:[],
                        selected:'',
                        choices:[{
                            value:'',
                            id:0
                        }]
                    }
                },
                computed:{
                    choices_id(){
                        return this.choices.length
                    }
                },
                watch:{
                    type:function(val){
                        this.getAnswerTypeView(val);
                    },
                    options:function(){
                        this.updateOpts();
                    }
                }, 
                methods:{
                    getAnswerTypeView(val){
                        switch(val){
                            case 'List':
                               this.getList()
                            break;
                            case 'Multichoice':
                                this.getMultichoice()
                            break;
                            case 'Prose':
                                this.getProse()
                            break;
                            default:
                                return '<div>&nbsp;</div>'
                        }
                    },
                    getList(){
                        var self = this
                        axios.get(url.origin+'/prep/manager/survey/getLists').
                        then(function(response){
                            self.options = response.data;
                        }).catch(function(error){
                            return error
                        })
                    },
                    getMultichoice(){
                        var self = this
                        self.options = '[]';

                    },
                    getProse(){
                        var self = this
                        self.options = '[]';
                    },
                    updateOpts(){
                        var self=this;
                        this.$emit('update-options', this.options);
                    },
                    updateListOption(e){
                        var self=this;
                        this.$parent.$emit('updateListParent', this.selected)
                        console.log(e)
                    },
                    addChoice(e){
                        var self=this;
                        this.choices.push({value:'',id:this.choices_id})
                        //this.$parent.$emit('updateChoices', e)
                    },
                    updateChoices(e){
                        var self= this;
                        this.choices[e.target.id].value = e.target.value;
                    }
                },
                props:['type','typesList'],
                created(){
                    this.selected = this.typesList
                    this.choices
                },
                template:'<div></div>',
            })



//Vue JS
var survey_edit = new Vue({
    el: '#survey_edit',
    data:{
        multipleChoices:[],
        options:[],
        answerTypeList:'sdfdf',
        answerTypeLabel:'',
        answerType:'Prose',
        s_description:'',
        s_title:'',
        response_msg:'',
        response_status:'',
        visibility:true,
        survey_id:window.location.pathname.split("/").pop(),
        questions:[]
    },
    components:{name:'answer-type-options', name:'list-component', name:'choice-component'},
    created(){
        this.$on('updateListParent', (selection) =>{
            this.answerTypeList = selection;
        }),

        this.$on('updateChoices', (choices) => {
            this.multipleChoices = choices;
        })
    },
    mounted(){
        this.fetchQuestions(this.questions);
    },
    methods:{
        addQuestion(){

            //Get survey id
            survey_id = this.$refs.survey_id.value;

            //Get form data
            var self = this;
            var form = document.getElementById('form_edit_survey');
            var formData = new FormData(form);

            //Send form data 
            axios.post(url.origin+'/prep/manager/survey/updateSurvey/'+survey_id, formData).
            then(function(response){
                self.response_msg = response.data.message;
                self.response_status = 'alert alert-'+response.data.status;

                setTimeout(()=>{
                    self.visibility = false,
                    self.response_msg ='';
                    self.response_status = '';
                    self.$watch('answerType', function(val){self.getAnswerTypeView(val)}, {immediate:true})

                }, 2000);

            }).catch(function(error){
                self.response_msg = error.message,
                self.response_status = 'alert alert-danger'                
            })

        },
        saveQuestion(){
            //get this variable
            var self = this;

            //get form data
            var form = document.getElementById('saveQuestion');
            var formData = new FormData(form);
            
            //Send form data
            axios.post(url.origin+'/prep/manager/survey/saveQuestion/'+survey_id, formData).then(function(response){
                self.response_msg = response.data.message;
                self.response_status = 'alert alert-'+response.data.status;
                setTimeout(()=>{
                    self.response_msg = '';
                    self.response_status ='';
                }, 1000)
            })

            //fetch questions again, i
            self.fetchQuestions(self.questions);
        },
        getAnswerTypeView(val){
            
            //set this to variable self
            var self = this;
            self.answerTypeLabel = val;


        },
         updateOptions(e){
             this.options = e;
         },
         updateListParent(e){
             this.answerTypeList = e.target.value
             console.log(this.answerTypeList)
         },
         updateChoices(e){
             this.multipleChoices.push(e);
         },
         fetchQuestions(questions){
             var self = this;
             axios.get(url.origin+'/prep/manager/survey/getAllQuestions/'+self.survey_id).then(function(response){
               self.questions = response.data
             }).catch(function(){

             })
         },
         fetchLists(){

         },

    }
});
